# L'algorithme de consentement v0.1

Le but de l'algorithme est de permettre de déboucher sur des propositions promulguées.

* Un utilisateur peut créér un brouillon de proposition.
* Un utilisateur peut demander à publier un brouillon de proposition.
* Un modérateur peut valider un brouillon de proposition et lancer sa publication.

## Cycle de vie d'une proposition Prop (publiée)

```
  Si publication initiale {
  	Prop.NuméroPhase = 1 // Clarification
  }

  Phase.durée = D

  Un autre utilisateur peut émettre un feedback
  Le proposeur peut répondre au feedback
  
  // Note : le traitement des feedbacks est phase-spécifique
  Si le créateur de feedback est convaincu par réponse|suite au feedback {
  	Le feedback est levé (et ne reste plus en suspend)
  }
  
	Si expiration(D) et Prop.Nb(Feedbacks en suspend) == 0 {
		Prop.NuméroPhase = Prop.NuméroPhase + 1

  		Si Prop.NuméroPhase == IndexPhaseFinale {
  			La proposition est promue
		}
	}

}

```

```
Cycle de vie d'une proposition promulguée {
	TODO
}
```

## Critères pour les transitions de phase

Critères temporels

* Crit T1 : une phase est délimitée dans le temps [ DateDePublication, + N jours ]

Critères conditionnels

* Crit C1 : tant qu'il subsiste des feedbacks non traités pour la phase actuelle : ne pas autoriser la transition.

### Références

* [gestion du consentement](https://universite-du-nous.org/wp-content/uploads/2013/09/gpc-2017-v0.1.pdf)


### Autres idées soumises à discussion

Si une objection n'est pas levée sans amendement au bout d'un certain temps, elle donne naissance à une contre-proposition
ou bien à une levée de l'objection.


### Evolutions remarquables

* 30-07-19 : fusion des phases clarification & réaction, choix effectué par les usagers initiaux.

