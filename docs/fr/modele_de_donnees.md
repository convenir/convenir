
**Utilisateur**

```
  login
  password
  role : { admin, moderateur, utilisateur }
```

**ContentSegment**

```
  type : [ text, image, video ]  
  content : string    // for text or base64 encoded images  
  external_url string // for content externally hosted, typically videos  
  internal_url string // after upload
```

**Proposition**

```
  auteur(s)
  content_segments : list de ContentSegment
  categories
  mot-clés
  version
  état : [  
    - crée (brouillon)
    - soumise pour modération
    - phase1
    - phase2
    - phase3
    - phase4 
    - acceptée
    - archivée
    - annulée
```

**Feedback**
```
  auteur
  type : demande clarification, question, objection, avis positif ou négatif
  état de traitement (répondu ? TODO spécifier) 
```

**Réponse**

```
  auteur  : string 
  contenu : string
```

TODO détailler comment s'articule cette notion d'Audition avec le reste :

**Audition**

```
  lieux 
  propositions abordées
  remarques faites (relation inverses)
```

