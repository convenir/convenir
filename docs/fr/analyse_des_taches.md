
## **Analyse des tâches**

Rôle : utilisateur standard  

* créer un brouillon de proposition, personnel, éditable sur une durée indéterminée.
* soumettre un brouillon de proposition (pour validation par modérateur)

* lister les propositions

* voir une proposition "courte" : retourne une représentation courte d'une proposition dans sa dernière version.

* voir une proposition "complète" : idem à "courte" + historique du feedback + réponses + versions intermédiaires.

Rôle : modérateur  

* revoir et valider un brouillon de proposition 
    refuser (idéalement avec msg explicatif)
    valider la soumission : la proposition entre en phase 1

* lister les demandes à modérer (clarifications, questions, amendements)
