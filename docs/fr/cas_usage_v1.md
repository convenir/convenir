## Scénario de cas d'usage basique

* Création d'utilisateurs de test : 1 admin, 1 modérateur *mod01*, 2 utilisateurs *contrib01*, *user01*

* *contrib01* : créer un brouillon de proposition (titre, texte), dans l'état "non publié".

* tout utilisateur peut :  
  - lister les propositions
  - afficher une proposition

* *contrib01* :  
  - édite son brouillon de proposition
  - soumet sa proposition pour validation avant publication
  - ou pourrait annuler sa soumission en cours (pour revenir en état brouillon).
  - ou pourrait Retirer sa proposition

* *mod01* :  
  - Lister les propositions devant être validées
  - Afficher une proposition
  - Valider ou refuser une proposition

* *user01* :  
  - Lister les propositions
  - Afficher une proposition
  - Donner du feedback à une proposition (clarification/question/objection) en fonction de la phase courante.

* *mod01* : 
  - Lister les feedbacks en attente de validation
  - Valider un feedback

* *contrib01* : 
  - Peut remarquer l'apparition de nouveaux feedbacks sur sa page d'accueil
  - Peut consulter un feedback (clarification question, objection, amendement...)

* *Processus récurrent* :
  - Sélectionne les phases en cours d'expiration, et les transitionne si les critères sont respectés.
