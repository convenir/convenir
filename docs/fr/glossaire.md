
* Amendement : un amendement est une demande de modification sur une proposition qui devrait donner naissance à une nouvelle version de celle-ci ou à une nouvelle proposition distincte.

* Demande de clarification : équivalent à "je n'ai pas compris, expliquez svp la partie où..."

* Feedback : un commentaire, une réaction d'accord/désaccord, une question, une objection.

* Proposeur : auteur d'une proposition

* Proposition : description d'une idée et de ses détails, avec éventuellement des illustrations (images, vidéos).

* Réaction (picto) : un moyen de donner du crédit (soutenir) ou du désaccord sur une publication.

* Réaction (verbalisée) : un commentaire suite à une réaction du type "j'ai compris mais j'aurai exprimé comme suit..."
