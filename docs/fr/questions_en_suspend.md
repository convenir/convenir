
## Questions en suspend

* Empêcher le contenu vide ou faible

Un auteur recevant des demandes de feedback pourrait produire des réponses stupides et voir sa proposition progresser vers la célébration.

Quels garde-fous pour juguler ce genre d'occurrence ?

Suggestion thio : autoriser les utilisateurs à apprécier la qualité des feedbacks et des réponses (+/-1)

* Utilisateurs  
    * Serait-il désirable de stimuler la participation en assignant un score d'engagement à chaque utilisateur ?
    * Est-ce qu'un niveau de confiance affecté à un utilisateur pourrait aider réduire le travail de modération de son feedback ?

* Quand une réaction a été déposée sur une proposition : comment signaler l'évènement à l'auteur,

   * intra app : bandeau en haut de l'UI
   * ou : envoi email, notif via mobile/IM app...

* Est-ce qu'une proposition en phase N+1 peut être rétrogradée en phase N ?

* Est-ce que l'identité des auteurs  de propositions ou de demandes est visible ?

   * Si oui, quelle est elle ? Nom réel, pseudonyme, identifiant numérique ?
   Réponse 30/07 : fonctionner de manière anonyme, permet de limiter les préjugés ou le crédit qu'on peut avoir pour une personne identifiable.

* Processus de modération  
    * Est-il attendu que les feedbacks soient tous modérés ?

* Phase "objections" : si une objection est opposée, quel est le critère qui détermine que l'objection est levée : suffit-il d'une réponse de l'auteur de la proposition ou faut-il que l'objecteur signale "objection levée" s'il/elle a été convaincu par la réponse de l'auteur de la proposition ?

* Phase "amendements" : idem que pour les objections, comment sont-ils traités ?

Autres questions moins prioritaires
    
**Général**  
   - le service sera t il public/ouvert  ou reservé à des membres  ?
   - compte identifiés a l interieur de l app, mais non relié a un nom ou un mail  
    - risques d attaques (tech, troll)
    
**Propositions**  
    - y a t il besoin de separer les propositions simples de celles plus complexes  
    - certains type de propositions ont elles des besoins tech specifiques.

## Interactions avec les publications

Conserve-t-on le bouton soutien mitigé (~) ?
