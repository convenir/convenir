## Fonctionnement d'une plateforme de co-construction par le consentement

Imaginez une plateforme web où vous pourriez interagir anonymement en créant et en discutant des propositions.

Tout utilisateur peut créer une proposition consistant en du texte et des images/vidéos support, et ensuite en devenir l'auteur.

Une fois créée une proposition peut être soumise pour validation avant publication : un modérateur la passera en revue et la validera ou la refusera, idéalement avec un message explicatif.

Une fois publiée, une proposition va avoir un cycle de vie au travers d'une série de phases : 

Phase 1 **CLARIFICATIONS**  : d'autres utilisateurs peuvent donner du feedback en :

* demandant des clarifications.
* donnant un avis positif ou négatif.

Phase 2 **OBJECTIONS** : d'autres utilisateurs peuvent donner du feedback en dressant des objections.

Phase 3 **AMENDEMENTS** : d'autres utilisateurs peuvent donner du feedback en proposant des amendements.

Un amendement peut provenir :  
- du proposeur
- d'un membre

Effet recherché : un relecteur s'exprimerais en déclarant "Si vous faisiez ça (cet amendement) vous auriez mon soutien".

## Transitions de phase en phase

Chaque phase a une durée déterminée, après laquelle, si toutes les requêtes de feedback ont été répondues alors la proposition peut être transitionnée vers la prochaine phase.

Quand la dernière phase arrivent à complétion, où tous les amendements ont été pris en compte [TODO spécifier] : la proposition entre célébration : état où le consentement collectif a été atteint.

Par proposition on entend tout assemblage de texte et de médias web qui décrivent une idée et son fonctionnement détaillé. Après être validée par un modérateur, une proposition est visible des autres utilisateurs qui peuvent ensuite donner du feedback pour idéalement tendre vers la célébration de la proposition où le consentement collectif a été atteint.

Une proposition a un cycle de vie en plusieurs phases durant lequel, elle va s'enrichir de feedbacks et évoluer, de version en version, pour prendre en compte les demandes de clarification, les objections et les amendements. 

## Modération

Quel est l'étendue du travail des modérateurs ?
* sur toutes les catégories
* sur une catégorie
* sur une ou plusieurs catégories


## Validation des propositions

La validation est utile pour assurer le dédoublonnage et la détection de contenu indésirable.

Le dédoublonnage permet de faire connaître au proposeur qu'une proposition similaire existe.

