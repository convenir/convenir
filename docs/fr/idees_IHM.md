
## Page d'accueil des propositions

* Mise en avant des propositions les plus soutenues

## Présentation d'une proposition

Compteurs :  
* de plus
* de moins
* de versions
* d'amendements

## Validation d'une proposition par un modérateur

Penser à fournir un message prédéfini dans une liste, ex: 
- une ou plusieurs proposition(s) similaire(s) existe.
- votre publication n'est pas conforme avec nos règles d'utilisation (contenu indésirable).


## Visualisation

Barre de progression affichant le cycle de vie de la proposition et ses données vitales (nb feedbacks...).

Affichage en branches d'une proposition

```
Prop1

|-- amendement : prop1.1
|
|-- amendement : prop1.2
|
|...
```
