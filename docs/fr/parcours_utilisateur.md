
# Parcours utilisateur

## Inscription

*ACC-SMS* : Confirmation d'inscription par code envoyé par SMS

## Page d'accueil

VIS-CRED : On aimerait mettre en avant les propositions les plus soutenues.

## Création de proposition

## Interactions avec une proposition

* S'abonner/suivre une proposition

* Bouton signe moins : signifie le désaccord. S'il est cliqué cela déclenche une saisie de contre demande, ce afin d'éviter le désaccord gratuit.

## Interactions avec commentaires

Ok pour boutons : Haut/Bas pour signifier feedback de soutien/désaccord

## Phase de votation

La votation est l'issue du vote de proposition promulguée.

