
Mardi 30 juillet :
* Finalisation traduction et encodage markdown des spécifications initiales.
* Recherche sur vue.js & vuetify (thio)
* Recherche sur l'encodage des opérations disponibles sur ressource par rôle.

Lundi 29 juillet :
* prise de connaissance des documents existants
* inventaire des entités logiques
* inventaire fonctionnel des rôles et tâches
* réflexions de design.
* démarrage des traductions
* setup gitlab, goup + dépôt initial
