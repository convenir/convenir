# Histoire du projet *Convenir*

Le projet a démarré suite à des discussions avec une équipe impliquée dans un projet citoyen qui a besoin d'une plateforme capable d'aider leur communauté à canaliser les contributions de leur grand nombre d'utilisateurs et faciliter la prise de décision.

Autant le projet cherche à se conformer aux exigences fixées par les utilisateurs initiaux, il est aussi conçu en tant qu'API en premier lieu pour pouvoir être réutilisé dans d'autres contextes.

L'analyse, la spécification, et l'implémentation préliminaire ont démarré lundi 29 juillet et vont durer 5 jours avec au moins un développpeur à plein temps.
