# Project *convenir*

*convenir* is a digital democracy initiative to equip large groups of people with a consent-based decision making platform where users can create proposals and discuss them.


In consensus the desired goal is that everyone says yes. In consent the desired outcome is that no one says no.



By proposal we mean a document mixing text and web media that describes an idea and its detailed functioning. After being validated by a moderator, a proposal is visible by the other users who can then give feedack for clarifications, question or object to it, in order to converge towards the celebration of the proposal where collective consent has been reached.

A proposal has a life cycle going through several phases during which it will be enriched by feedback from other users and evolve, from version to version, to take into account requests for clarification, questions, objections and amendments.  

This document is a work in progress that is both a functional requirements analysis and the premices of an API specification that will serve implementors.

References in english:

* [consent-responsibility](https://www.sociocracy.info/consent-responsibility)
* [consensus, consent, objections](https://www.sociocracy.info/consensus-consent-objections)

References in french:

* [gestion du consentement](https://universite-du-nous.org/wp-content/uploads/2013/09/gpc-2017-v0.1.pdf)

## History and status of the project

The project started out of discussions with a team involved in a project for social change that needs a platform capable of helping their community to channel the contributions of a large number of users.

As much as the project tries to fulfill the requirements set by the initial stakeholders, it is also designed as an API first so that it can be reused in other contexts.


The analysis, specification, and draft implementations started on monday 29th july and shall last for 5 days with at least one fulltime developer.

There is indeed plenty of various tasks for several people and your help is welcome on this activities :

* Reviewing our docs and spotting inconsistencies, underspecified areas, badly formulated sentenced, bad english, typos...
* Reviewing and commenting the design decisions
* Help with the reference implementation of the API (not started yet)
* Help us with a test frontend : current plans would be to use web components (vue.js, aurelia, polymer...)

And in the future, helping on :
* alternate frontends or integrations
* alternate implementations of the API (GraphQL, GRPC...)

Anyway, for the time being everything here is WIP, so expect mistakes here and there.

## Overview of the "consent algorithm"

Picture a web platform where you could interact anonymously by creating or debating proposals.
Any user can create a proposal consisting of text and supporting images and videos, and thus become an author.

Once created a proposal can be submitted for publishing : a moderator will review the proposal contents and validate or refuse it, ideally with an explanatory message.


Once published, a proposal is going to have a lifecycle going through a series of phases : 

Phase 1 : CLARIFICATION  
=> Other users can give feedback by asking for clarifications.

Once a given duration has elapsed, and all requests for clarifications have been fulfilled : the proposal transitions to phase 2.


Phase 2 : REACTION
=> Other users can give feedback by asking questions.

Once a given duration has elapsed, and all feedbacks have been replied to : the proposal transitions to phase 3.

Phase 3 : OBJECTION  
=> Other users can give feedback by raising objections.

Once a given duration has elapsed, and all objections have been taken into account : the proposal transitions to phase 4.

Phase 4  : ENACTMENT  
=> Other users can give feedback by requesting amendments.

Once a given duration has elapsed, and all amendments have been taken into account : the proposal transitions to celebration, a state where collective consent has been reached.

## Questions still pending

* Users
    * Would it be desirable to stimulate user engagement by giving them a level of involvment ?
    * Would a level of trust assigned to a user be useful to reduce the need for moderating his/her feedback ?

* When a user gives feedback to a proposal : how should the proposal author be informed ?
   * in app : banner message in frontend
   * or : email notif, mobile/IM app notification...

* Can a proposal in phase N+1 be downgraded to phase N ?

* Shall an identity of the author of a proposal or a feedback be visible ?
  
   * If yes, which would it be ? Real name, pseudonym, or a numeric id ?

* Moderation process
    * Is it expected that all moderators validate all comments ?

* Objection phase : if an objection is opposed, what is the criterium that determines when the objection is dropped : would an answer from the author suffice or it is necessary for the objector to be convinced by the answer and in turn signal its intent to drop the objection ?

* General
    * If some areas of the platform are made public, which are they ? 
    * Shall the platform allow search bots ?
    * How does the platform cope with bad behaviour like troll users ?

## Task analysis

Role : standard user  

* create a draft proposal, personal, editable over an indefinite period.
* submit a draft proposal for validation by a moderator.

* list proposals

* view proposal "short" : display contents of a proposal in its latest version.

* view proposal "full" : same as short + history of all feedback and versions of the proposal.

Role : moderator  

* review a proposal and 
    refuse (with ideally an explanatory message)
    validate a submitted proposal : the proposal transitions to phase 1

* list feedbacks that need to be moderated (request for clarification, questions, objections, amendments)

## Basic use case scenario

* Creation of test users : 1  admin, 1 moderator : *modo01*, 2 users : *contrib01* et *user01*

* *contrib01* : Create draft proposal (title, texte..), state : unpublished.

* any user can : 
  - List proposals
  - Display a proposal  

* *contrib01* : 
  - Edit a (draft) proposal
  - Submit proposal  
  - or may Cancel a pending proposal submission (to return draft state)
  - or may Remove proposal

* *modo01* : 
  - List proposals that need to be reviewed
  - Display a proposal
  - Validate or Refuse a proposal

* *user01* : 
  - List proposals
  - Display proposal
  - Give feedback to proposal (clarification/question/objection depending on current phase)

*contrib01* :
  - Can notice new feedbacks to his/her proposal on his homepage
  - Can view a feedback
  - Can reply  to a feedback

* *modo01* : 
  - Lists feedbacks pending validation
  - Validates feedback

* Long lived process : 
  - selects expiring proposal phases, transitions proposals to next phase if they match criteria for transitioning.


## Data model (WIP)

**User**  
```
  login  
  password  
  role : { admin, moderator, user }
```

**ContentSegment**
```
  type : [ text, image, video ]  
  content : string    // for text or base64 encoded images  
  external_url string // for content externally hosted, typically videos  
  internal_url string // after upload
```

**Proposal**  
```
  author  
  content_segments : list of ContentSegment  
  category  
  keywords  
  version  
  state : [  
    - draft  
    - submitted for moderation  
    - phase1  
    - phase2  
    - phase3  
    - phase4  
    - accepted  
    - archived  
    - cancelled   
  ]
```

**Feedback**
```
  author
  type : [ request for clarification, question, objection, positive, negative]
```

## Description of processings

### Criteria for transitioning proposals from one phase to another

Temporal criteria :

   * Crit T1 : a phase has a limited duration [ PublishingDate, + N days ]

Conditional criteria :

   * Crit C1 : a proposal can not be transitioned when there are remaining unprocessed feedbacks

So in its simplest incarnation : a transition can be done when :
    IF Elapsed(Duration of phase) AND Count(Replies) == Count(Feedback) THEN
       Transition Proposal to next phase
    EndIF

## Minimal testbench UI overview

TODO

## Misc UI/UX ideas

Progression bar displaying the lifecycle of a proposal and its vital data

## Project history

Tuesday 30th july :

Monday 29th july :

* review of existing user documents and existing specs/implementations
* inventory of entities
* functional task analysis
* design discussions
* translation of the docs (french, english)
* setup of the gitlab group + initial repository
