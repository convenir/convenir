# Projet 'convenir'

*convenir* est une initiative de démocratie numérique pour équiper un grand groupe de personnes d'un moyen de prise de décision par le consentement leur permettant de créer des propositions et de les discuter en plusieurs étapes.

Dans un consensus le but recherché est que tout le monde dise oui : difficilement atteignable dans un grand groupe de personnes. 
Dans le consentement l'aboutissement désiré est que personne ne dise non.

Ce projet est un travail en cours qui est à la fois une analyse des besoins, des prérequis fonctionnels et les prémices d'une spécification d'API à destination des implémenteurs.

STATUT : débroussaillage, spécs, design, prototypage.

## Documents de travail

* La spécification de [l'algorithme de consentement](docs/fr/algo_consentement.md)

* [Fonctionnement d'une plateforme de décision par consentement](docs/fr/fonctionnement_plateforme.md)
* [Parcours utilisateur](docs/fr/parcours_utilisateur.md)

* [Analyse des tâches](docs/fr/analyse_des_taches.md)

* [Cas d'usage V1](docs/fr/cas_usage_v1.md)

* [Modèle de données](docs/fr/modele_de_donnees.md)

## Comment participer

Pour comprendre d'où vient et où en est le projet vous devriez jeter un coup d'oeil à sa jeune [histoire](docs/fr/history).

Il y a en effet plein de tâches différentes pour plusieurs personnes et votre aide est la bienvenue sur ces activités :

* Revue des docs et recherche d'incohérences, parties sous spécifiées, phrases mal formulées, mauvais français, coquilles...
* Revue et commentaires des discussions de design.
* Aide au développement sur l'implémentation de  référence de l'API (pas encore démarré).
* Aide au développement avec une interface web de test : les plans actuels regardent du côté de composants web (vue.js, aurelia, polymer...)

* des frontends alternatifs ou des intégrations
* des implémentations alternatives de l'API (GraphQL, GRPC...)

Dans tous les cas, pour le temps présent, tout ce que vous trouverez ici est WIP (Travail en cours), alors attendez-vous à des erreurs ici et là.

